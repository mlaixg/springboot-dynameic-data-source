package com.zlg.demo.service;

import java.util.List;

import com.zlg.demo.config.DataBase;
import com.zlg.demo.entity.User;
import com.zlg.demo.mapper.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;


    @Override
    @DataBase("db1")
    public List<User> getAllOrclUsers() {
        List<User> users = userDao.queryAllWithSlave();
        return users;
    }

    @Override
    @DataBase("db2")
    public List<User> getAllMySQLUsers() {
        List<User> users = userDao.aaa();
        return users;
    }

}
