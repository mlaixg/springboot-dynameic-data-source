package com.zlg.demo.service;

import com.zlg.demo.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;



public interface UserService {
    public List<User> getAllOrclUsers();
    public List<User> getAllMySQLUsers();
}

