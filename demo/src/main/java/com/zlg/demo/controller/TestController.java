package com.zlg.demo.controller;


import com.zlg.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("teat")
@RestController
public class TestController {

    @Autowired
    UserService userService;


    @GetMapping("test1")
    public Object test1() {
        return userService.getAllMySQLUsers();
    }

    @GetMapping("test2")
    public Object test2() {
        return userService.getAllOrclUsers();
    }
}
